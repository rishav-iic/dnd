import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles";
import { Flipped } from "react-flip-toolkit";

import {
  ItemRendererProps,
  useDrag,
  useDrop,
  useIsClosestDragging
} from "react-sortly";

type ItemItemRendererProps = ItemRendererProps<{
  name: string;
  credit: number;
}>;
const useStyles = makeStyles<
  Theme,
  { muted: boolean; depth: number; credit: number }
>((theme: Theme) => ({
  root: props => ({
    position: "relative",
    marginBottom: theme.spacing(1.5),
    zIndex: props.muted ? 1 : 0
    // padding: '0 !important'
  }),
  body: props => ({
    background: "white",
    cursor: "move",
    padding: 5,
    marginLeft: theme.spacing(props.depth * 2),
    boxShadow: props.muted ? "0px 0px 8px #666" : "0px 0px 2px #666",
    border: props.muted ? "solid 1px #3d3a67" : "1px solid transparent",
    // height: "40px",
    borderRadius: "8px"
  }),
  circle: props => ({
    height: 25,
    width: 25,
    backgroundColor: props.credit == 2 ? "#08e200" : "cornflowerblue",
    borderRadius: "50%",
    display: "inline-block"
  }),
  rectangle: props => ({
    width: "250px",
    height: "100px",
    padding: "7px",
    color: "white",
    borderRadius: "8%",
    background: props.credit % 2 === 1 ? "#08e200" : "cornflowerblue"
  })
}));

const DefaultItemRenderer = (props: ItemItemRendererProps) => {
  const {
    id,
    depth,
    data: { name, credit }
  } = props;
  const [{ isDragging }, drag] = useDrag({
    collect: monitor => ({
      isDragging: monitor.isDragging()
    })
  });
  const [, drop] = useDrop();
  const classes = useStyles({
    muted: useIsClosestDragging() || isDragging,
    depth,
    credit
  });
  return (
    <Flipped flipId={id}>
      <div ref={ref => drop(ref)} className={classes.root}>
        <div ref={drag} className={classes.body}>
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <span
                  className={classes.circle}
                  style={{
                    fontSize: 10,
                    padding: 6,
                    color: "#fff",
                    fontWeight: 500,
                    textAlign: "center"
                  }}
                >
                  {credit}
                </span>
              </div>
              <div className="col-md-7" style={{ padding: 0 }}>
                <span style={{ fontSize: 12 }}>{name}</span>
              </div>
              <div className="col-md-3" style={{ padding: 0 }}>
                <span className={classes.rectangle} style={{ fontSize: 9 }}>
                  DSE - 1
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Flipped>
  );
};

export default DefaultItemRenderer;
