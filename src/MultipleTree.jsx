import React from "react";
import { Box } from "@material-ui/core";
import update from "immutability-helper";
import { useDrop } from "react-dnd";
import Sortly, { add, remove, findDescendants } from "react-sortly";
import DefaultItemRenderer from "./DefaultItemRenderer";
import useScreenSize from "./useScreenSize";
import CourseModal from "./Modal";

const CATEGORIES = [
  {
    id: 1,
    name: "Semster 1",
    credits: 0,
    items: [],
    options : [{
      type : "group",
      credit : {min : 2, max : 10},
      options : [
        {
          id: 1,
          categoryId: 1,
          name: "Basic Circuit Theory Network analysis",
          depth: 0,
          credit: 4,
          type: "Core",
          courseCode: "EC-201"
        },
        {
          id: 2,
          categoryId: 1,
          name: "Basic Circuit Theory Network analysis",
          depth: 0,
          credit: 2,
          type: "core",
          courseCode: "EC-201"
        },
      ]
    }]
  },
  {
    id: 2,
    name: "Semster 2",
    credits: 0,
    items: [],
    options : []
  },
  {
    id: 3,
    name: "Semster 3",
    credits: 0,
    items: [],
    options : []
  },
  {
    id: 4,
    name: "Semster 4",
    credits: 0,
    items: [],
    options : []
  },
  {
    id: 5,
    name: "Semster 5",
    credits: 0,
    items: [],
    options : []
  },
  {
    id: 6,
    name: "Semster 6",
    credits: 0,
    items: [],
    options : []
  },
  {
    id: 7,
    name: "Course",
    items: [
      {
        id: 1,
        categoryId: 7,
        name: "Basic Circuit Theory Network analysis",
        depth: 0,
        credit: 4,
        type: "Core",
        courseCode: "EC-201"
      },
      {
        id: 2,
        categoryId: 7,
        name: "Basic Circuit Theory Network analysis",
        depth: 0,
        credit: 2,
        type: "core",
        courseCode: "EC-201"
      },
      {
        id: 3,
        categoryId: 7,
        name: "Basic Circuit Theory Network analysis",
        depth: 0,
        credit: 4,
        type: "elective",
        courseCode: "EC-201"
      },
      {
        id: 4,
        categoryId: 7,
        name: "Basic Circuit Theory Network analysis",
        depth: 0,
        credit: 4,
        type: "elective",
        courseCode: "EC-201"
      },
      {
        id: 5,
        categoryId: 7,
        name: "Basic Circuit Theory Network analysis",
        depth: 0,
        credit: 4,
        type: "elective",
        courseCode: "EC-201"
      }
    ]
  }
];

const Tree = ({ id, items, onChange, onEnter, value,showDesc }) => {
  const [{ hovered, dropPlace, dragItem }, drop] = useDrop({
    accept: "TREE",
    collect: monitor => ({
      hovered: monitor.isOver(),
      dragItem: monitor.getItem(),
      dropPlace: monitor.isOver()
    })
  });

  const handleMove = React.useCallback(() => {
    if (!dragItem) {
      return;
    }

    if (hovered) {
      onEnter(dragItem);
    }
  }, [dragItem, hovered, onEnter]);

  React.useEffect(() => {
    if (dragItem) {
      handleMove();
    }
  }, [dragItem, hovered, handleMove]);

  return (
    <div
      ref={drop}
      style={styles.dashedBox}
      className="sortly"
    >
      {(!items.length && showDesc) && <p style={styles.dropDesc}>
        To Add, Drag and drop course here from Course List
      </p>}
      <Sortly type="TREE" items={items} onChange={onChange}>
        {DefaultItemRenderer}
      </Sortly>
    </div>
  );
};

const MultipleTree = () => {
  const { isLargeScreen } = useScreenSize();
  const [categories, setCategories] = React.useState(CATEGORIES);
  const [finalResult, setFinalResult] = React.useState();
  const [toggleState, setToggleState] = React.useState(false);

  function toggle() {
    setToggleState(toggleState === false ? true : false);
  }


  let sum = 0;
  categories.forEach((item, i) => {
    if (i == 6) {
      item.items.forEach(item => {
        sum += item.credit;
      });
    }
  });

  const handleChange = index => newItems => {
    setCategories(
      update(categories, {
        [index]: { items: { $set: newItems } }
      })
    );
  };

  const handleEnter = targetCategoryIndex => dragItem => {
    const sourceCategoryIndex = categories.findIndex(category =>
      category.items.some(item => item.id === dragItem.id)
    );
    const sourceCategory = categories[sourceCategoryIndex];
    const targetCategory = categories[targetCategoryIndex];

    if (targetCategory.items.some(item => item.id === dragItem.id)) {
      return;
    }

    const sourceItemIndex = sourceCategory.items.findIndex(
      item => item.id === dragItem.id
    );
    const sourceItem = sourceCategory.items[sourceItemIndex];
    const sourceDescendants = findDescendants(
      sourceCategory.items,
      sourceItemIndex
    );

    const items = [sourceItem, ...sourceDescendants].map(item => ({
      ...item,
      categoryId: targetCategory.id
    }));

    setCategories(
      update(categories, {
        [sourceCategoryIndex]: {
          items: { $set: remove(sourceCategory.items, sourceItemIndex) }
        },
        [targetCategoryIndex]: {
          items: { $set: add(targetCategory.items, items) }
        }
      })
    );
    
    Fulldetails(items);
    setFinalResult(items);
  };

  const Fulldetails = items => {
    const result = items;
    const categoryID = items[0].categoryId;
    const targetSemster = categories[categoryID - 1];
  };

  const addDraggableArea = (item) => {
    console.log(item);
    toggle();
  }

  const dropArea = () => (
    <Tree
      id={categories.length - 1}
      items={categories[categories.length - 1].items}
      onChange={handleChange(categories.length - 1)}
      onEnter={handleEnter(categories.length - 1)}
      showDesc={false}
    />
  )

  const renderDropAreas = (options) => (
    options.map((item, ind) => {
      return dropArea();
    })
  )

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-8" style={{ display: "flex", flexWrap: "wrap" }}>
          {categories.map(({ id, name, items, options }, index) => {
            if (id == categories.length) {
              return null;
            }
            return (
              <div className="col-md-5" style={styles.mdstyle}>
                <Box key={id} pr={4} mb={4} style={{ width: "100%" }}>
                  <div id="textbox">
                    <p style={styles.style2}>
                      {name}
                    </p>
                    <p style={styles.style1}>
                      Credits
                    </p>
                  </div>
                  <div style={{ clear: "both" }}></div>
                  <Tree
                    id={id}
                    items={items}
                    onChange={handleChange(index)}
                    onEnter={handleEnter(index)}
                    showDesc={true}
                  />
                </Box>
                <button onClick={toggle}>Toggle Modal</button>
               <CourseModal id={id} addDraggableArea={addDraggableArea} backgroundisOpen={toggleState} onClose={toggle} isOpen={toggleState}/>
                {/* <Tree
                  id={id}
                  items={items}
                  onChange={handleChange(index)}
                  onEnter={handleEnter(index)}
                  showDesc={true}
                /> */}
                {renderDropAreas(options, id, items)}
              </div>
            );
          })}
        </div>
        <div className="col-md-4" style={styles.courseStyle}>
          <Box
            key={categories.length - 1}
            width={"100%"}
            pr={4}
            mb={4}
            style={styles.box}
          >
            <div id="textbox">
              <p
                style={styles.style2}
              >
                {categories[categories.length - 1].name} List
              </p>
              <p
                style={styles.style1}
              >
                Credits : {sum}
              </p>
            </div>
            <div style={{ clear: "both" }}></div>
            <Tree
              id={categories.length - 1}
              items={categories[categories.length - 1].items}
              onChange={handleChange(categories.length - 1)}
              onEnter={handleEnter(categories.length - 1)}
              showDesc={false}
            />
          </Box>
        </div>
      </div>
    </div>
  );
};

const styles = {
  mdstyle: {
    margin: 20,
    border: "solid 1px #3d3a67",
    backgroundColor: "aliceblue"
  },
  courseStyle: {
    borderRadius: "8px",
    backgroundColor: "aliceblue",
    marginTop: 15
  },
  style1: {
    float: "right",
    color: "cornflowerblue",
    fontWeight: 600,
    fontSize: 16
  },
  style2: {
    float: "left",
    fontSize: 20,
    fontWeight: 600
  },
  dropDesc : {fontSize:10, textAlign:'center', marginTop: 10},
  dashedBox : {
    padding: 10,
    paddingBottom: 5,
    backgroundColor: "#fff",
    border: "1px dashed black"
  },
  box : { width: "auto", padding: 0 }
};

export default MultipleTree;
