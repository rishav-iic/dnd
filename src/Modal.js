import React, {Component} from "react";
import "./modal.css";
import Modal from "react-responsive-modal";

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center",
  width: "60%"
};

export default class CourseModal extends Component {

  state = { value: "list", min: "", max : "" };

  handleChange = (type) => {
    this.setState({
      value: type,
      min : 0,
      max : 0
    })
  };

  inputChange = event => {
    this.setState({
      min: event.target.value
    });
  };

  inputChange2 = event => {
    this.setState({
      max: event.target.value
    });
  };

  onSubmit = () => {
    this.props.addDraggableArea({option : this.state , id : this.props.id });
  };

  isButtonDisabled = () => {
    return !this.state.min || !this.state.max
  }

  render() {
    return (
      <div style={styles}>
        <Modal
          classNames="abc"
          open={this.props.isOpen}
          onClose={() => this.props.onClose()}
          styles={{ backgroudColor: "green" }}
        >
          <h5>Select Elective Subject Type</h5>
          <div className="row">
            <div className="col-md-6">
              <div class="row">
                <div class="col-md-4">
                  <label>
                    <input
                      type="radio"
                      value="list"
                      checked={this.state.value === "list"}
                      onChange={() => {
                        this.handleChange("list");
                      }}
                    />{"  "}
                    From a List
                  </label>
                </div>
                <div class="col-md-4">
                  <label>
                    <input
                      onChange={this.inputChange}
                      value={this.state.value === "list" && this.state.min || ""}
                      type="text"
                      className="form-control"
                      placeholder="Min Credit"
                      disabled={this.state.value !== "list"}
                    />
                  </label>
                </div>
                <div class="col-md-4">
                  <input
                    onChange={this.inputChange2}
                    value={this.state.value === "list" && this.state.max || ""}
                    type="text"
                    className="form-control"
                    placeholder="Max Credit"
                    disabled={this.state.value !== "list"}
                  />
                </div>
              </div>
              <div
                style={{
                  backgroundColor: "grey",
                  width: "100%",
                  height: "150px"
                }}
              ></div>
            </div>
            <div className="col-md-6">
              <div class="row">
                <div class="col-md-4">
                  <label>
                    <input
                      type="radio"
                      value="group"
                      checked={this.state.value === "group"}
                      onChange={() => {
                        this.handleChange("group");
                      }}
                    />{"   "}
                    Group
                  </label>
                </div>
                <div class="col-md-4">
                  <label>
                    <input
                      onChange={this.inputChange}
                      value={this.state.value === "group" && this.state.min || ""}
                      type="text"
                      className="form-control"
                      placeholder="Min Credit"
                      disabled={this.state.value !== "group"}
                    />
                  </label>
                </div>
                <div class="col-md-4">
                  <input
                    onChange={this.inputChange2}
                    value={this.state.value === "group" &&this.state.max || ""}
                    type="text"
                    className="form-control"
                    placeholder="Max Credit"
                    disabled={this.state.value !== "group"}
                  />
                </div>
              </div>
              <div
                style={{
                  backgroundColor: "grey",
                  width: "100%",
                  height: "150px"
                }}
              ></div>
            </div>
            <button disabled={this.isButtonDisabled()} className={"btn btn-primary mt-2 mx-auto"} style={{width: "200px"}} onClick={() => this.onSubmit()}>Add Course</button>
          </div>
        </Modal>
      </div>
    );
  }
}
